# Lista de comandos de Git



## Configuración Inicial

Si el equipo es nuevo o se acaba de instalar git, lo primero que hay que realizar es la configuración del usuario y correo asociado.



**Nombre**:

```
git config --global user.name "FIRST_NAME LAST_NAME"
```

**Correo**:

```
git config --global user.email "MY_NAME@example.com"
```



* Nota: Es probable que el equipo ya se haya configurado con otra cuenta, al ejecutar los comandos, estos se sobreescribiran. Si deseas listar el nombre o correo actual puedes utilizar:

  ```
  git config user.name
  git config user.email
  ```





## Generar una llave SSH

Generar una llave ssh, permite al repositorio(gitlab, github, bitbucket) saber que el equipo de donde se esta haciendo el **push** es un equipo en la _lista blanca_. En pocas palabras realizar el handcheck del equipo y el repositorio.



#### Generar la llave en sistemas Unix

Abre una terminal y teclea lo siguiente:

recuerda usar el correo asociado a tu equipo

```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

Esto te realizara una serie de preguntas, que puedes dejar en blanco, u optar por poner una contraseña como la ruta donde se guardara el archivo **id_rsa.pub**.

![](src/img/key.png)

la ruta por defecto es en _~/.ssh/_ 



#### Agregando la llave a un repositorio

Esto puede cambiar un poco de repositorio a repositorio, pero en escénica es lo mismo, para este ejemplo usaremos **Github**.

dentro de _settings/profile_ se encuentra un menu en el lateral izquierdo, da click en **SSH and GSP keys** > Nueva key.

![](src/img/github.png)



1. Teclea un nombre para la llave, por ejemplo _serviciosQA_.
2. copia el contenido de **id_rsa.pub**.
3. Guarda.

Si estas en unix puedes listar el contenido desde la terminal usando el comando _cat_.

```
cat ~/.ssh/id_rsa.pub
```



![](src/img/github2.png)



si esto se realizo correctamente, ahora es probable clonar un proyecto usando **ssh**.



## Clonando Proyectos

Para clonar un proyecto, abre la terminal y teclea:

```
git clone *proyecto*
```

ejemplo:

```
git clone git@gitlab.com:juand-ik/git-command-line.git
```



### Comandos de git



#### Listar estado

Listando cambios, para conocer el estatus actual de cambios teclea:

```
git status
```

Esto marcara en rojo los archivos, que han sido modificados y aun estan fuera de git, como la _rama_ en la que te encuentras.

![](src/img/gitstatus.png)



#### Agregando archivos al scope de git



Abre la terminal y teclea:

```
git add nombre_archivo.ext
```

ejemplo:

```
git add README.md
```

* Nota: Es posible agregar diferentes archivos en la misma linea separados por espacios.

```
git add README.md inst.txt
```

Es posible agregar todos los archivos utilizando:

```
git add .
```

Más ejemplos:

* **git add "*.txt"**      <- Agrega todos los archivos del proyecto con la extensión indicada.
* **git add \*.txt**        <- Agrega todos los archivos del directorio actual con la extensión indicada.
* **git add --all**        <- Agrega todos los archivos
* **git add folder/*.js**  <- Agrega todos los archivo con la extensión(js,txt, html, etc...) dentro del directorio indicado.
* **git add folder/**      <- Agrega todos los archivo dentro del directorio indicado.



#### Crando un mensaje - commit



Una vez que se agregaron los archivos al scope(git add), lo siguiente es crear un commit, que es como un snapshot del proyecto actual.

```
git commit -m "initial commit"
```

También es posible agregar todos los archivos y el commit en una sola linea.

```
git commit -am "initial commit"
```

#### Crando un alias para los comandos


Para crear un alias en git teclea, git config --global alias.`nombre_a_usar` "**comando a ejecutar**"

Ejemplo:
```
git config --global alias.lg "log --oneline --decorate --all --graph"
```
